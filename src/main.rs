use std::env;

fn fizzbuzz(n: u32) -> String {
    if n % 3 == 0 && n % 5 == 0 {
        return "FizzBuzz".to_string();
    } else if n % 3 == 0 {
        return "Fizz".to_string();
    } else if n % 5 == 0 {
        return "Buzz".to_string();
    }
    n.to_string()
}

fn main() {
    let arg: String = match env::args().nth(1) {
        Some(arg) => arg,
        None => panic!("Error: No input!"),
    };
    let n: u32 = match arg.parse() {
        Ok(n) => n,
        Err(_) => panic!("Error parsing input."),
    };

    // Make the number range inclusive using =
    for i in 1..=n {
        println!("{}", fizzbuzz(i));
    }
}

#[test]
fn test_fizz() {
    let result = fizzbuzz(9);
    assert_eq!("Fizz", result);
}

#[test]
fn test_buzz() {
    let result = fizzbuzz(10);
    assert_eq!("Buzz", result);
}

#[test]
fn test_fizzbuzz() {
    let result = fizzbuzz(15);
    assert_eq!("FizzBuzz", result);
}

#[test]
fn test_number() {
    let result = fizzbuzz(7);
    assert_eq!("7", result);
}
